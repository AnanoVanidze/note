package com.example.notesapp


import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_add_notes.*

class AddNotesActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_notes)



        saveButton.setOnClickListener {
            saveNote()
        }
    }


    private fun saveNote() {

        val returnIntent = Intent()
        if (titleET.text.toString().isNotEmpty() && notesET.text.toString().isNotEmpty()) {
            val note = Note(titleET.text.toString(), notesET.text.toString())
            sendData(note)
            returnIntent.putExtra("note", note)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }
    }

    private fun sendData(note: Note) {
        val uid = FirebaseAuth.getInstance().uid
        database = Firebase.database.reference
        if (uid != null) {
            database.child("note").child(uid).setValue(note)
        }

    }

}

