package com.example.notesapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signIn.setOnClickListener {
            signIn()
        }


    }


    private fun signIn() {
        val email = emailET.text.toString()
        val password = passwordET.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()) {
            signIn.isClickable = false
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    signIn.isClickable = true
                    if (task.isSuccessful) {
                        d("signIn", "signInWithEmail:success")
                        Toast.makeText(this, "Authentication is Success!", Toast.LENGTH_SHORT)
                            .show()
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                        val user = auth.currentUser
                    } else {
                        d("signIn", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            this, task.exception.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
        } else {
            Toast.makeText(this, "Please, fill all fields", Toast.LENGTH_SHORT).show()
        }
    }


}