package com.example.notesapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.note_item_layout.view.*

class NotesAdapter : RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    val noteItems = mutableListOf<Note>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.note_item_layout, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val noteItem = noteItems[position]
        holder.itemView.titleTV.text = noteItem.title
        holder.itemView.descriptionTV.text = noteItem.description
    }

    override fun getItemCount(): Int {
        return noteItems.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}