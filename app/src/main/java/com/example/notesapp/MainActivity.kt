package com.example.notesapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.note_item_layout.*

class MainActivity : AppCompatActivity() {
    private lateinit var database: DatabaseReference
    private var notesAdapter = NotesAdapter()
    private val REQUEST_CODE = 1
    private var result: Note? = null
    private var notesArray = mutableListOf<Note>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        notesRecyclerView.layoutManager = LinearLayoutManager(this)
        notesRecyclerView.adapter = notesAdapter
        notesAdapter.noteItems.addAll(notesArray)
        notesAdapter.notifyDataSetChanged()
        readNote()

    }

    private fun init() {


        addButton.setOnClickListener() {
            val intent = Intent(this, AddNotesActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE)
        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE)
            if (resultCode == Activity.RESULT_OK) {
                result = data?.getParcelableExtra("note")
                result?.let { notesAdapter.noteItems.add(addNote(it)) }
                notesAdapter.notifyDataSetChanged()
            } else
                Toast.makeText(this, "note was not saved successfully", Toast.LENGTH_SHORT).show()
    }

    private fun addNote(note: Note): Note {
        return note
    }
    private fun readNote(){
        database = Firebase.database.reference
        database.addChildEventListener(object : ChildEventListener{
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                snapshot.children.forEach {
                    val note = it.value
                    d("note",note.toString())
//                    if (note != null) {
//                        notesArray.add(0,note)
//                        notesAdapter.notifyDataSetChanged()
//                    }
                }

            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })







    }
}