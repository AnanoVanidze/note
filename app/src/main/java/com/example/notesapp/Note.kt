package com.example.notesapp

import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize
@IgnoreExtraProperties
@Parcelize
class Note(val title:String = "", val description:String = ""): Parcelable

